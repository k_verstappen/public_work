#!/usr/bin/python

"""
Python Docker Pull/Remove Script
Author: Kevin Verstappen
Date: 13-3-2017
"""

from ansible.module_utils.docker_common import *

try:
	from docker.auth.auth import resolve_repository_name
	from docker.utils.utils import parse_repository_tag
except ImportError:
	pass

# Het importeren van alle Ansible / Python module functionaliteiten omtrent Docker.

class ImageManager(DockerBaseClass):
# Het definieren van een klasse genaamd "ImageManager" die gebaseerd (inheritance) is op de klasse "DockerBaseClass".
	
	def __init__(self, client, results):
# Het definieren en initieren van een nieuwe instantie van de klasse "ImageManager" met verschillende referenties naar de ImageManger klasse.
		
		super(ImageManager, self).__init__()
# Het definieren van een super op de klasse ImageManager. Dit wordt gedaan omdat eigenschappen van de 'parent' van ImageManager (DockerBaseClass) moeten worden overgenomen.

		self.client = client
		self.results = results
		parameters = self.client.module.params
		self.check_mode = self.client.check_mode
		self.name = parameters.get('name')
		self.tag = parameters.get('tag')
		self.force = parameters.get('force')
		self.path = parameters.get('path')
		self.state = parameters.get('state')
		self.archive_path = parameters.get('archive_path')
		self.repository = parameters.get('repository')
		self.pull = parameters.get('pull')
		self.push = parameters.get('push')
		self.load_path = parameters.get('load_path')
# Definitie van de verschillende aan te roepen functies binnen de klasse ImageManager.

		repo, repo_tag = parse_repository_tag(self.name)
		if repo_tag:
			self.name = repo
			self.tag = repo_tag
# Overrule van tag wanneer deze wordt ingegeven in de YAML file.

		if self.state in ['present', 'build']:
			self.present()
		elif self.state == 'absent':
			self.absent()
# Afhankelijk van de state present of absent wordt de  bijbehorende functionaliteit afgetrapt. LET OP -> Argument 'Build' is deprecated en wordt verwijderd in Ansible v2.3.

	def fail(self, msg):
		self.client.fail(msg)
# In het geval van een 'fail' wordt de bijbehorende 'msg' meegegeven.

	def present(self):
		image = self.client.find_image(name=self.name, tag=self.tag)
# Definitie van de 'present' a.k.a. 'pullen' van een image. Een image is gedefinieerd als zijnde 'zoek image op basis van naam & tag'.
		
		if not image or self.force: # Indien image of force parameter niet is ingegeven,
			if self.path:
				if not os.path.isdir(self.path): # Wanneer de OS dir niet overeenkomt met het pad (self.path), fail dan incl. onderstaande error msg.
					self.fail("Requested build path %s could not be found or you do not have access." % self.path)
				image_name = self.name # Wanneer image_name wordt aangeroepen, definieer deze dan gelijk aan self.name.
				if self.tag: # Wanneer een eigen tag is ingevoerd, koppel deze dan in een string aan de 'image_name' en voer dan -->
					image_name = "%s:%s" % (self.name, self.tag)
				self.log("Building image %s" % image_name)																#	self.log uit met bijbehorende bericht.
				self.results['actions'].append("Built image %s from %s" % (image_name, self.path))						#	resultaten met actie(s) waaraan een bericht wordt toegevoegd.
				self.results['changed'] = True																			#	geef aan dat resultaten veranderd zijn met 'True'.
				if not self.check_mode:	# Indien de gebruiker geen test run uitvoert, voer dan -->
					self.results['image'] = self.build_image() # 									# Het bouwen van het image uit.
			elif self.load_path: # Anders wanneer 'self.load_path' ->
				if not os.path.isfile(self.load_path):					# Wanneer de 'self.load_path' niet gelijk is aan 'os.path.isfile' ->		
					self.fail("Error loading image %s. Specified path %s does not exist." % (self.name, self.load_path))	# geef dan met een error aan dat het opgegeven pad niet bestaat.

				image_name = self.name # 'image_name' is gelijk aan 'self.name'
				if self.tag:
					image_name = "%s:%s" % (self.name, self.tag) # Wanneer een eigen tag is ingevoerd, koppel deze dan in een string aan de 'image_name' en voer dan -->
				self.results['actions'].append("Loaded image %s from %s" % (image_name, self.load_path)) 	# Resultaten van de 'actions' waaraan een bericht is toegevoegd.
				self.results['changed'] = True 																# Geef aan dat de resultaten veranderd zijn met 'True'.
				if not self.check_mode: # Indien niet de gebruiker geen test run uitvoert voer dan -->	
					self.results['image'] = self.load_image()											# Het laden van een image uit op basis van 'self.name & self.tag'.

			else:
				self.results['actions'].append('Pulled image %s:%s' % (self.name, self.tag)) # Wanneer 'build' of 'load' niet werkt, voer dan het 'pullen' van een image uit.
				self.results['changed'] = True # Geef aan dat de reusltaten veranderd zijn met 'True'.
				if not self.check_mode: # Indien de gebruiker geen test run uitvoert voer dan -->	
					self.results['image'] = self.client.pull_image(self.name, tag=self.tag)			# De pull image functionaliteit uit waarin de opgegeven naam en tag wordt ingegeven. 
		
		if self.archive_path:
			self.archive_image(self.name, self.tag) # Wanneer het archief pad wordt ingegeven, koppeld deze dan aan self.archive_image middels de self.name en self.tag.

		if self.push and not self.repository: # Wanneer er wordt gepusht maar geen repository wordt meegegeven -->		
			self.push_image(self.name, self.tag)																		# Koppel de naam en tag dan aan elkaar.
		elif self.repository: # Wanneer de repository wel wordt ingegeven, voer dan de functionaliteit 'tag_image' uit met de bijbehorende velden.
			self.tag_image(self.name, self.tag, self.repository, force=self.force, push=self.push)	

	def absent(self): # Definieer de functie voor het verwijderen van een image.
		image = self.client.find_image(self.name, self.tag) # Wanneer image wordt aangeroepen, zoek dan naar de koppeling vna de betreffende image naam en tag.
		if image: # Wanneer image is gevonden
			name = self.name # Koppel dan 'name' aan 'self.name'
			if self.tag: # Wanneer de 'self.tag' is opgegeven -->
				name = "%s:%s" % (self.name, self.tag) # Koppel deze dan aan de naam.
			if not self.check_mode: # Indien het geen test run betreft binnen Ansible, probeer dan -->
				try:
					self.client.remove_image(name, force=self.force) # Het verwijderen van een image op basis van (name == self.name)
				except Exception as exc: # Behalve wanneer -->
					self.fail("Error removing image %s - %s" % (name, str(exc))) # Een error voordoet, geef dan aan wat de naam en de Exception (error msg) is.

			self.results['changed'] = True # Geef aan dat de reusltaten veranderd zijn met 'True'.
			self.results['actions'].append("Removed image %s" % (name)) # Voeg de naam toe van welk image is verwijderd.
			self.results['image']['state'] = 'Deleted' # Geef aan dat de image is veranderd middels absent, daar deze ook gelijk is aan 'deleted'.

def main(): # Dit is het YAML PlayBook gedeelte waarin YAML haar (mandatory) values uithaalt.
	argument_spec = dict( # Open een dictionary waarbij elke entry een value heeft gekoppeld, zoals pull (type=bool) 
		force=dict(type='bool', default=False),
		archive_path=dict(type='path'),
		pull=dict(type='bool', default=True),
		repository=dict(type='str'),
		load_path=dict(type='path'),
		push=dict(type='bool', default=False),
		path=dict(type='path', aliases=['build_path']),
		name=dict(type='str', required=True),
		tag=dict(type='str', default='latest'),
		state=dict(type='str', choices=['absent', 'present', 'build'], default='present'),
	)

	client = AnsibleDockerClient(
		argument_spec=argument_spec, # Is gelijk aan elkaar
		supports_check_mode=True, # De AnsibleDockerClient ondersteund test runs.
	)		

	results = dict(		# De resultaten worden weergegeven middels een dictionary
		changed=False,
		actions=[],
		image={}
	)

	ImageManager(client, results)
	client.module.exit_json(**results)

from ansible.module_utils.basic import *

if __name__ == '__main__':
	main()